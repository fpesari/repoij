#!/usr/bin/ruby
# frozen_string_literal: true

# SPDX-License-Identifier: AGPL-3.0-or-later

require 'erb'
require 'octokit'
require 'opml-parser'
require 'open-uri'
require 'nokogiri'
require 'pathname'
require 'repomd_parser'
require 'rss'
require 'semantic'
require 'toml'

module Repoij
  module Interface
    module Terminal
      class << self
        def parse_args
          { primary_xml: ARGV[0], db: ARGV[1], repo_name: ARGV[2] }
        end

        def main
          args = parse_args
          repo = RPM::Repository.new(args[:primary_xml])
          db = Database.new(args[:db])
          puts Report::Generator::HTML.new(
            Report::Generator.new(repo, db, args[:repo_name])
          ).text
        end
      end
    end
  end

  module RPM
    class Repository
      attr_reader :packages

      def initialize(index_filename)
        # Excluded suffixes or prefixes
        @exclude = ENV['EXCLUDE']&.split ||
                   %w[32bit bash-completion common devel doc fonts lang]
        # For packages with multiple variants
        @strip = ENV['STRIP']&.split ||
                 %w[alsa ladspa dssi jack lv2 vst vst3]
        @packages = cleanup(
          RepomdParser::PrimaryXmlParser.new(index_filename).parse.map do
            [_1.name, _1.version]
          end.to_h
        )
      end

      private

      def cleanup(db)
        no_excluded = db.reject do |k, _v|
          k.match?(/-?(#{@exclude.join '|'})-?/)
        end
        # Remove duplicates
        no_excluded.each_with_object({}) do |(k, v), a|
          kc = k.dup
          @strip.each do
            kc.gsub!("-#{_1}", '')
            kc.gsub!("#{_1}-", '')
          end
          a[kc] ||= v
        end.sort.to_h
      end
    end
  end

  class Database
    attr_reader :entries

    def initialize(filename)
      @entries = TOML.load(File.read(filename))
    end
  end

  module Report
    class Generator
      VERSION_REGEXP = /(?<version>\d+\.\d+\.?\d*\.?\d*)/.freeze
      attr_reader :repo_name, :report, :n_updated

      def initialize(repository, database, repo_name)
        @repo_name = repo_name
        gh_client = Octokit::Client.new(access_token: ENV['GITHUB_TOKEN'])
        rpm_packages = repository.packages
        db_entries = database.entries
        mapping = db_entries.select { |_k, v| v[repo_name] }
                            .transform_values { |v| v[repo_name] }

        @report = rpm_packages.each_with_object({}) do |(k, v), a|
          mapped = mapping[k]
          in_db = db_entries.find do |kk, _vv|
                    k.downcase == kk.downcase
                  end&.first
          next a unless mapped || in_db

          item = db_entries[mapped ? k : in_db]
          next a if item['unver']

          upstream_version = if item['source'] == 'github'
                               begin
                                 gh_client.tags(
                                   item['github']
                                 ).select do
                                   _1[:name].match VERSION_REGEXP
                                 end.map { extract_version _1[:name] }&.max do
                                   Semantic::Version.new(_1) <=>
                                     Semantic::Version.new(_2)
                                 end
                               rescue ArgumentError
                                 extract_version(
                                   gh_client.tags(item['github']).find do
                                     _1[:name].match VERSION_REGEXP
                                   end&.fetch(:name)
                                 ) || '-'
                               end

                             elsif (item['source'] == 'gitlab') &&
                                   !item['gitlab'].start_with?('http')
                               url = File.join 'https://gitlab.com',
                                               item['gitlab'],
                                               '/-/tags?format=atom'
                               URI.open(url) do |rss|
                                 feed = RSS::Parser.parse rss,
                                                          do_validate = false
                                 latest = feed.items.find do
                                   extract_version _1.title.to_s
                                 end
                                 version = extract_version latest.to_s
                                 next version ||
                                   Nokogiri::HTML(
                                     feed.items.first.title.to_s
                                   ).title
                               end
                             else
                               next a
                             end
          a[k] = { 'version_repo' => (extract_version(v) || v).strip,
                   'version_upstream' => (upstream_version&.strip || '-') }
          a
        end
        @n_updated = @report.select do |_k, v|
          v['version_repo'] == v['version_upstream']
        end.length
      end

      class TOML
        def initialize(generator)
          TOML::Generator.new(generator.report).body
        end
      end

      class HTML
        attr_reader :text

        def initialize(generator)
          @repo_name = generator.repo_name
          @report = generator.report
          @n_updated = generator.n_updated
          template = ERB.new File.read File.join(
            Pathname.new(__FILE__).dirname, 'templates', 'packages.erb'
          )
          @text = template.result binding
        end

        private

        def colorize(version_repo, version_upstream)
          begin
            f_version_repo = Semantic::Version.new version_repo
            f_version_upstream = Semantic::Version.new version_upstream
          rescue StandardError
            f_version_repo = version_repo
            f_version_upstream = version_upstream
          end
          if f_version_repo == f_version_upstream
            ' style="background-color: rgba(90, 210, 90, 122) !important;"'
          elsif f_version_repo >= f_version_upstream
            ' style="background-color: rgba(242, 164, 9, 122) !important;"'
          else
            ''
          end
        end
      end

      private

      def semversize(version)
        version&.gsub(/^(\d+\.\d+)$/, '\1.0')
      end

      def extract_version(text)
        semversize(text&.match(VERSION_REGEXP)
        &.named_captures&.fetch('version')&.gsub(/[[:punct:]]$/, ''))
      end
    end
  end

  module Feed
    module Parser
      module OPML
        def initialize; end
      end
    end
  end
end

Repoij::Interface::Terminal.main
