# repoij - Package update report generator for RPM

repoij is a project management tool which can verify if packages in a given RPM
repository are at their latest version compared to their official repositories
(for now, only Github and Gitlab repositories are supported).

It was written as part of the [GeekosDAW](https://geekosdaw.tuxfamily.org)
project (openSUSE-based repository for music-making) and with the feedback
of its community, for which I am extremely thankful.

## Usage

```
GITHUB_TOKEN=XYZ ./repoij PRIMARY DATABASE REPO_NAME
```

Where:
- `XYZ` is your personal Github API token.
- `PRIMARY` is the location of the `primary.xml.gz` file downloaded from a
RPM repository.
- `DATABASE` is the location of the TOML database written according to the
[nvchecker](#nvchecker) spec.
- `REPO_NAME` is the name of the RPM repository.

### nvchecker

Here are two sample entries according to the `nvchecker` schema:

```toml
[Geonkick]
gitlab = "iurie-sw/geonkick"
prefix = "v"
source = "gitlab"
use_max_tag = true

[GrandOrgue]
github = "GrandOrgue/grandorgue"
prefix = ""
source = "github"
use_max_tag = true
```

`prefix` is the prefix used in the releases. Many developers leave it empty,
some put things like `v` (i.e. `v1.2.3`) or `Version-` (i.e. `Version-1.2.3`).
It is arbitrarily chosen by each developer so make sure to check it manually.

## License

All files in this repository are released under the GNU Affero General Public
License, version 3 or any later version (AGPLv3+).